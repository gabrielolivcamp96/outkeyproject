# outkeyproject

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Considerações para o Teste Flutter - OUTKEY
Usuarios: 
        1.
        'cpf': 60060060060,
        'pass': 12345678,
        'name': 'Joaquim',
        'idade': 15,
        'permissao': 'L',
        2.
        'cpf': 60060060061,
        'pass': 12345678,
        'name': 'Marilia',
        'idade': 16,
        'permissao': 'L',
        3.
        'cpf': 60060060062,
        'pass': 12345678,
        'name': 'Cleber',
        'idade': 17,
        'permissao': 'L',
        4.
        'cpf': 60060060063,
        'pass': 12345678,
        'name': 'Carla',
        'idade': 18,
        'permissao': 'A',
        5.
        'cpf': 60060060064,
        'pass': 12345678,
        'name': 'Eduardo',
        'idade': 19,
        'permissao': 'A',
        6.
        'cpf': 60060060065,
        'pass': 12345678,
        'name': 'Alana',
        'idade': 20,
        'permissao': 'A',