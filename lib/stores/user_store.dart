import 'package:flutter/material.dart';

import '../models/user_model.dart';
import '../services/user_service.dart';

class UserStore extends ValueNotifier<UserModel> {
  final UserService service;

  UserStore(UserModel result, this.service) : super(result);

  Future fetchUser(String cpf) async {
    await Future.delayed(const Duration(seconds: 1));
    try {
      final user = await service.fetchUser(cpf);
      value = user;
      notifyListeners();
    } catch (e) {}
  }
}
