// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class UserModel {
  String cpf;
  String pass;
  String name;
  int idade;
  String permissao;

  UserModel({
    required this.cpf,
    required this.pass,
    required this.name,
    required this.idade,
    required this.permissao,
  });

  static Map<String, dynamic> toMap(UserModel user) {
    return <String, dynamic>{
      'cpf': user.cpf,
      'pass': user.pass,
      'name': user.name,
      'idade': user.idade,
      'permissao': user.permissao,
    };
  }

  factory UserModel.fromMap(Map<String, dynamic> map) {
    // print(map['items'][0]['logo_pic']);
    return UserModel(
      cpf: map['items'][0]['cpf'] as String,
      pass: map['items'][0]['pass'] as String,
      name: map['items'][0]['name'] as String,
      idade: map['items'][0]['idade'] as int,
      permissao: map['items'][0]['permissao'] as String,
    );
  }

  // String toJson() => json.encode(toMap());

  factory UserModel.fromJson(String source) =>
      UserModel.fromMap(json.decode(source) as Map<String, dynamic>);

  static List<UserModel> toList(List<Map<String, dynamic>> mapaDeUsers) {
    print('Convertendo to List:');
    final List<UserModel> tarefas = [];
    for (Map<String, dynamic> linha in mapaDeUsers) {
      final UserModel tarefa = UserModel(
        cpf: linha['cpf'],
        pass: linha['pass'],
        name: linha['name'],
        idade: linha['idade'],
        permissao: linha['permissao'],
      );
      tarefas.add(tarefa);
    }
    print('Lista de cpfs: ${tarefas.toString()}');
    return tarefas;
  }
}
