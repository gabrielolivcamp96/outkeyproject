import 'package:outkeyproject/data/user_dao.dart';

import '../models/user_model.dart';

class UserAuth {
  static authLogin(String cpf, String pass) async {
    await UserDao().findAll();
    List<UserModel> result = await UserDao().find(cpf);
    UserModel user = result[0];

    if (pass == user.pass) {
      return [true];
    } else {
      return false;
    }
  }
}
