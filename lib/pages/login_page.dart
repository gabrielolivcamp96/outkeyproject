import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:brasil_fields/brasil_fields.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:outkeyproject/models/user_model.dart';
import 'package:provider/provider.dart';

import '../components/text_form_login_widget.dart';
import '../controllers/login_controller.dart';
import '../data/user_dao.dart';
import '../routes/app_routes.dart';
import '../stores/user_store.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  bool _popup = false;
  bool isLoading = false;
  final LoginController _controller = LoginController();
  final TextEditingController _passController = TextEditingController();
  final TextEditingController _loginController = TextEditingController();

  _showPopUp(String mensagemErro) {
    return AwesomeDialog(
      context: context,
      width: 360,
      headerAnimationLoop: false,
      animType: AnimType.bottomSlide,
      title: 'Informação',
      autoDismiss: true,
      btnOkOnPress: () {},
      btnOk: TextButton.icon(
        onPressed: () {
          setState(() {
            _popup = false;
          });
          Navigator.pop(context);
        },
        icon: const Icon(Icons.account_circle),
        label: const Text(
          'Ok',
          style: TextStyle(fontSize: 20),
        ),
      ),
      desc: mensagemErro,
      descTextStyle: const TextStyle(fontSize: 20),
    ).show();
  }

  @override
  Widget build(BuildContext context) {
    final userStore = context.watch<UserStore>();
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/backgroundapp.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Padding(
          padding: EdgeInsets.only(top: size.height * 0.2),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Image.asset(
                  'assets/logo.png',
                  width: 100,
                  // height: 100,
                ),
                Form(
                  key: _formKey,
                  child: Padding(
                    padding: EdgeInsets.only(
                      top: 25,
                      left: size.width * 0.03,
                      right: size.width * 0.03,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextFormLoginWidget(
                          validator: (value) {
                            if (value!.length < 11) {
                              if (!_popup) {
                                setState(() {
                                  _popup = true;
                                });
                                _showPopUp('CPF inválido.');
                              }
                              return 'CPF inválido.';
                            }
                            return null;
                          },
                          cor: Colors.blue.shade900,
                          icon: Icon(
                            Icons.perm_identity,
                            size: 23,
                            color: Colors.blue.shade900,
                          ),
                          label: 'CPF',
                          hint: '000.000.000-00',
                          textInput: TextInputType.number,
                          controller: _loginController,
                          onChangeController: _controller.setLogin,
                          inputFormatters: [
                            // obrigatório
                            FilteringTextInputFormatter.digitsOnly,
                            FilteringTextInputFormatter.deny(' '),
                            CpfInputFormatter(),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormLoginWidget(
                          validator: (value) {
                            if (value == null ||
                                value.isEmpty ||
                                value.length < 8) {
                              if (!_popup) {
                                setState(() {
                                  _popup = true;
                                });
                                _showPopUp('Por favor informe senha válida.');
                              }
                              return 'Por favor insira uma senha válida.';
                            }
                            return null;
                          },
                          controller: _passController,
                          cor: Colors.blue.shade900,
                          icon: Icon(
                            Icons.vpn_key_rounded,
                            color: Colors.blue.shade900,
                          ),
                          label: 'Senha',
                          isObscure: true,
                          textInput: TextInputType.text,
                          showButtom: true,
                          onChangeController: _controller.setSenha,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter(
                              RegExp(
                                r'(\s)$',
                                multiLine: false,
                                caseSensitive: false,
                              ),
                              allow: false,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 25, left: 35, right: 35),
                  child: ElevatedButton(
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all(const StadiumBorder()),
                        minimumSize: MaterialStateProperty.all(
                            const Size.fromHeight(55)),
                        backgroundColor:
                            MaterialStateProperty.all(Colors.red.shade600)),
                    child: isLoading
                        ? const CircularProgressIndicator(
                            color: Colors.white, strokeWidth: 2)
                        : const Text(
                            'Entrar',
                            style: TextStyle(fontSize: 20, color: Colors.white),
                          ),
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        setState(() => isLoading = true);
                        await _controller.login().then((result) async {
                          if (result[0]) {
                            setState(() {
                              isLoading = false;
                              _controller.setLogin;
                              _passController.clear();
                            });
                            await context
                                .read<UserStore>()
                                .fetchUser(_loginController.text);

                            // ignore: use_build_context_synchronously
                            Navigator.pushReplacementNamed(
                              context,
                              AppRoutes.INDEX,
                            );
                          } else {
                            setState(() => isLoading = false);
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                content: Text(result[1]),
                                duration: const Duration(seconds: 4),
                              ),
                            );
                          }
                        });
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
          // FutureBuilder(
          //     future: UserDao().findAll(),
          //     builder: (context, snapshot) {
          //       List<UserModel>? items = snapshot.data;
          //       switch (snapshot.connectionState) {
          //         case ConnectionState.none:
          //           return Center(
          //             child: Column(
          //               children: const [
          //                 CircularProgressIndicator(),
          //                 Text('Carregando'),
          //               ],
          //             ),
          //           );

          //         case ConnectionState.waiting:
          //           return Center(
          //             child: Column(
          //               children: const [
          //                 CircularProgressIndicator(),
          //                 Text('Carregando'),
          //               ],
          //             ),
          //           );
          //         case ConnectionState.active:
          //           return Center(
          //             child: Column(
          //               children: const [
          //                 CircularProgressIndicator(),
          //                 Text('Carregando'),
          //               ],
          //             ),
          //           );
          //         case ConnectionState.done:
          //           if (snapshot.hasData && items != null) {
          //             if (items.isNotEmpty) {
          //               return SingleChildScrollView(
          //                 child: Column(
          //                   children: [
          //                     Form(
          //                       key: _formKey,
          //                       child: Padding(
          //                         padding: EdgeInsets.only(
          //                             top: size.height * 0.4,
          //                             left: size.width * 0.02,
          //                             right: size.width * 0.02),
          //                         child: Column(
          //                           crossAxisAlignment: CrossAxisAlignment.center,
          //                           mainAxisAlignment: MainAxisAlignment.center,
          //                           children: [
          //                             TextFormLoginWidget(
          //                               controller: _loginController,
          //                               validator: (value) {
          //                                 if (!EmailValidator.validate(value!)) {
          //                                   if (!_popup) {
          //                                     setState(() {
          //                                       _popup = true;
          //                                     });
          //                                     _showPopUp(
          //                                         'Por favor informe um email válido.');
          //                                   }
          //                                   return 'Email inválido.';
          //                                 }
          //                                 return null;
          //                               },
          //                               icon: Icon(
          //                                 Icons.person_pin_circle_rounded,
          //                                 color: Colors.blue.shade900,
          //                                 size: 27,
          //                               ),
          //                               cor: Colors.blue.shade900,
          //                               label: 'Email',
          //                               textInput: TextInputType.emailAddress,
          //                               onChangeController: _controller.setLogin,
          //                               inputFormatters: <TextInputFormatter>[
          //                                 FilteringTextInputFormatter(
          //                                   RegExp(
          //                                     r'(\s)$',
          //                                     multiLine: false,
          //                                     caseSensitive: false,
          //                                   ),
          //                                   allow: false,
          //                                 ),
          //                               ],
          //                             ),
          //                             const SizedBox(
          //                               height: 10,
          //                             ),
          //                             TextFormLoginWidget(
          //                               validator: (value) {
          //                                 if (value == null ||
          //                                     value.isEmpty ||
          //                                     value.length < 8) {
          //                                   if (!_popup) {
          //                                     setState(() {
          //                                       _popup = true;
          //                                     });
          //                                     _showPopUp(
          //                                         'Por favor informe senha válida.');
          //                                   }
          //                                   return 'Por favor insira uma senha válida.';
          //                                 }
          //                                 return null;
          //                               },
          //                               controller: _passController,
          //                               cor: Colors.blue.shade900,
          //                               icon: Icon(
          //                                 Icons.vpn_key_rounded,
          //                                 color: Colors.blue.shade900,
          //                               ),
          //                               label: 'Senha',
          //                               isObscure: true,
          //                               textInput: TextInputType.text,
          //                               showButtom: true,
          //                               onChangeController: _controller.setSenha,
          //                               inputFormatters: <TextInputFormatter>[
          //                                 FilteringTextInputFormatter(
          //                                   RegExp(
          //                                     r'(\s)$',
          //                                     multiLine: false,
          //                                     caseSensitive: false,
          //                                   ),
          //                                   allow: false,
          //                                 ),
          //                               ],
          //                             ),
          //                           ],
          //                         ),
          //                       ),
          //                     ),
          //                     ElevatedButton(
          //                       style: ButtonStyle(
          //                           shape: MaterialStateProperty.all(
          //                               const StadiumBorder()),
          //                           minimumSize: MaterialStateProperty.all(
          //                               const Size.fromHeight(55)),
          //                           backgroundColor: MaterialStateProperty.all(
          //                               Colors.red.shade600)),
          //                       child: isLoading
          //                           ? const CircularProgressIndicator(
          //                               color: Colors.white, strokeWidth: 2)
          //                           : const Text(
          //                               'Entrar',
          //                               style: TextStyle(
          //                                   fontSize: 20, color: Colors.white),
          //                             ),
          //                       onPressed: () async {
          //                         if (_formKey.currentState!.validate()) {
          //                           setState(() => isLoading = true);
          //                           _controller.login().then((result) async {
          //                             if (result[0]) {
          //                               setState(() {
          //                                 isLoading = false;
          //                                 _controller.setLogin;
          //                                 _passController.clear();
          //                               });
          //                               await context
          //                                   .read<UserStore>()
          //                                   .fetchUser(_loginController.text);

          //                               Navigator.pushNamed(
          //                                   context, AppRoutes.INDEX);
          //                             } else {
          //                               setState(() => isLoading = false);
          //                               ScaffoldMessenger.of(context)
          //                                   .showSnackBar(
          //                                 SnackBar(
          //                                   content: Text(result[1]),
          //                                   duration: const Duration(seconds: 4),
          //                                 ),
          //                               );
          //                             }
          //                           });
          //                         }
          //                       },
          //                     ),
          //                   ],
          //                 ),
          //               );
          //             }
          //           }
          //       }
          //       return const Text('Erro desconhecido');
          //     }),
        ),
      ),
    );
  }
}
