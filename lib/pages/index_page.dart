import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:outkeyproject/controllers/update_user_controller.dart';
import 'package:outkeyproject/pages/splash_page.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

import '../routes/app_routes.dart';
import '../stores/user_store.dart';

class IndexPage extends StatefulWidget {
  const IndexPage({Key? key}) : super(key: key);

  @override
  State<IndexPage> createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  final StreamController<Position> _locStream = StreamController();
  late StreamSubscription<Position> locationSubscription;
  late Color textColor = Colors.blue;
  // final Permission _permission = Permission.location;
  // PermissionStatus _permissionStatus = PermissionStatus.denied;

  @override
  void initState() {
    super.initState();
    GeolocatorPlatform.instance.requestPermission();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      context.read<UserStore>().fetchUser(context.watch<UserStore>().value.cpf);
    });
    // startLocation();
  }

  @override
  void dispose() {
    _locStream.close();
    locationSubscription.cancel();

    super.dispose();
  }

  pauseLocation() async {
    locationSubscription.pause();
  }

  @override
  Widget build(BuildContext context) {
    final userStore = context.watch<UserStore>();
    final UpdateUserController _controller = UpdateUserController();
    final TextEditingController _nameController = TextEditingController();
    final TextEditingController _ageController = TextEditingController();
    _nameController.text = userStore.value.name;
    _ageController.text = userStore.value.idade.toString();

    startLocation() {
      final positionStream =
          Geolocator.getPositionStream().handleError((error) {});
      locationSubscription = positionStream.listen((Position position) {
        _locStream.sink.add(position);

        if (textColor == Colors.blue) {
          textColor = Colors.red;
        } else {
          textColor = Colors.blue;
        }
      });
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Index'),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.of(context)
                  .pushNamedAndRemoveUntil(AppRoutes.LOGIN, (route) => false);
            },
            icon: const Icon(
              Icons.logout,
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 50.0),
        child: Column(
          children: [
            CircleAvatar(
              backgroundColor: userStore.value.permissao == "A"
                  ? Colors.grey
                  : Color.fromARGB(255, 71, 137, 190),
              child: Text(
                userStore.value.name[0].toUpperCase(),
                style: TextStyle(
                  fontSize: 30,
                  color: userStore.value.permissao == "A"
                      ? Colors.white
                      : Colors.black,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Form(
                child: Column(
                  children: [
                    TextFormField(
                      controller: _nameController,
                      readOnly: userStore.value.permissao == "A" ? false : true,
                      onChanged: _controller.setName,
                    ),
                    TextFormField(
                      controller: _ageController,
                      onChanged: _controller.setAge,
                      readOnly: userStore.value.permissao == "A" ? false : true,
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Visibility(
              visible: userStore.value.permissao != "A" ? false : true,
              child: ElevatedButton.icon(
                onPressed: () async {
                  if (userStore.value.name != _nameController ||
                      userStore.value.idade != _ageController) {
                    await _controller.update(userStore.value).then((value) {
                      if (value) {}
                    });
                  }
                },
                icon: const Icon(Icons.refresh),
                label: Text('Salvar alterações'),
              ),
            ),
            SizedBox(
              height: 25,
            ),
            // ValueListenableBuilder(
            //     valueListenable: userStore,
            //     builder: (context, value, child) => Text(value.name)),
            // // Text(userStore.value.name),
            Text('Sua localização é:'),
            StreamBuilder(
              stream: _locStream.stream,
              builder: (context, snapshot) => Container(
                height: 70,
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Column(
                    children: [
                      Text(
                        snapshot.data == null
                            ? "Clique para carregar"
                            : snapshot.connectionState ==
                                    ConnectionState.waiting
                                ? "Carregando"
                                : snapshot.data.toString(),
                        style: TextStyle(color: textColor),
                      ),
                      ElevatedButton(
                          onPressed: () => startLocation(),
                          child: Text('Carregar localização')),
                    ],
                  ),
                ),
              ),
            ),

            // Text(userStore.value.name),
          ],
        ),
      ),
    );
  }
}
