import 'package:outkeyproject/models/user_model.dart';
import 'package:outkeyproject/services/user_service.dart';

class UpdateUserController {
  String? _name;
  setName(String value) => _name = value.trim();

  String? _age;
  setAge(String value) => _age = value.trim();

  Future update(UserModel user) async {
    int? idade = null;
    if (_age != null) {
      idade = int.parse(_age.toString());
    }
    return await UserService().updateUser(user, _name.toString(), idade);
  }
}
