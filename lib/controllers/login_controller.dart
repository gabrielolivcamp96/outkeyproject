import 'package:brasil_fields/brasil_fields.dart';
import 'package:flutter/widgets.dart';

import '../auth/user_auth.dart';
import '../services/user_service.dart';

class LoginController {
  ValueNotifier<bool> inLoader = ValueNotifier<bool>(false);

  String? _login;
  setLogin(String value) => _login = value.trim();

  String? _senha;
  setSenha(String value) => _senha = value.trim();

  Future<List> login() async {
    try {
      return await UserAuth.authLogin(
        UtilBrasilFields.removeCaracteres(_login!.toString()),
        _senha.toString(),
      );
    } catch (e) {
      return [false, 'Digite suas credenciais.'];
    }
  }
}
