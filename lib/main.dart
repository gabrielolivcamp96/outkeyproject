import 'package:flutter/material.dart';
import 'package:outkeyproject/pages/index_page.dart';
import 'package:outkeyproject/pages/login_page.dart';
import 'package:outkeyproject/pages/splash_page.dart';
import 'package:outkeyproject/routes/app_routes.dart';
import 'package:outkeyproject/stores/user_store.dart';
import 'package:provider/provider.dart';

import 'models/user_model.dart';
import 'services/user_service.dart';

void main() {
  runApp(
    const MyApp(),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(create: (context) => UserService()),
        ChangeNotifierProvider(
          create: (context) => UserStore(
            UserModel(cpf: '', idade: 0, name: '', pass: '', permissao: ''),
            context.read(),
          ),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        // home: LoginPage(),
        initialRoute: '/login',
        routes: {
          AppRoutes.LOGIN: (context) => LoginPage(),
          AppRoutes.INDEX: (context) => IndexPage(),
          // AppRoutes.SPLASH: (context) => SplashPage(),
        },
      ),
    );
  }
}
