import 'package:outkeyproject/data/user_dao.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

Future<Database> getDatabase() async {
  // final String path = join(await getDatabasesPath(), 'user.db');
  final String path = join(await getDatabasesPath(), 'user.db');
  return openDatabase(
    path,
    onCreate: (db, version) async {
      await db.execute(UserDao.tableSql);
    },
    version: 1,
  );
}
