import 'package:brasil_fields/brasil_fields.dart';
import 'package:outkeyproject/models/user_model.dart';
import 'package:sqflite/sqflite.dart';

import 'database.dart';

class UserDao {
  static const String tableSql = 'CREATE TABLE $_tablename('
      '$_cpf TEXT, '
      '$_pass TEXT, '
      '$_name TEXT, '
      '$_idade INTEGER, $_permissao TEXT);';

  static const String _tablename = 'userTable';
  static const String _cpf = 'cpf';
  static const String _pass = 'pass';
  static const String _name = 'name';
  static const String _idade = 'idade';
  static const String _permissao = 'permissao';

  // Crud
  // #### Save ####
  save(UserModel user) async {
    print('Iniciando o save: ');
    final Database bancoDeDados = await getDatabase();
    var itemExists = await find(user.cpf);
    Map<String, dynamic> UserMap = UserModel.toMap(user);
    if (itemExists.isEmpty) {
      print('O CPF não existia.');
      return await bancoDeDados.insert(_tablename, UserMap);
    } else {
      print('O CPF existia!');
      return await bancoDeDados.update(
        _tablename,
        UserMap,
        where: '$_cpf = ?',
        whereArgs: [user.cpf],
      );
    }
  }

  // #### Find All ####
  Future<List<UserModel>> findAll() async {
    print('Acessando o findAll: ');
    final Database bancoDeDados = await getDatabase();
    List<Map<String, dynamic>> result = await bancoDeDados.query(_tablename);
    if (result.isEmpty) {
      await bancoDeDados.insert('userTable', {
        'cpf': 60060060060,
        'pass': 12345678,
        'name': 'Joaquim',
        'idade': 15,
        'permissao': 'L',
      });
      await bancoDeDados.insert('userTable', {
        'cpf': 60060060061,
        'pass': 12345678,
        'name': 'Marilia',
        'idade': 16,
        'permissao': 'L',
      });
      await bancoDeDados.insert('userTable', {
        'cpf': 60060060062,
        'pass': 12345678,
        'name': 'Cleber',
        'idade': 17,
        'permissao': 'L',
      });
      await bancoDeDados.insert('userTable', {
        'cpf': 60060060063,
        'pass': 12345678,
        'name': 'Carla',
        'idade': 18,
        'permissao': 'A',
      });
      await bancoDeDados.insert('userTable', {
        'cpf': 60060060064,
        'pass': 12345678,
        'name': 'Eduardo',
        'idade': 19,
        'permissao': 'A',
      });
      await bancoDeDados.insert('userTable', {
        'cpf': 60060060065,
        'pass': 12345678,
        'name': 'Alana',
        'idade': 20,
        'permissao': 'A',
      });
      result = await bancoDeDados.query(_tablename);
    }
    print('Procurando dados no banco de dados... encontrado: $result');
    return UserModel.toList(result);
  }

  // #### Find ####
  Future<List<UserModel>> find(String cpf) async {
    print('Acessando find: ');
    cpf = UtilBrasilFields.removeCaracteres(cpf);
    final Database bancoDeDados = await getDatabase();
    print('Procurando CPF com o nome: ${cpf}');
    final List<Map<String, dynamic>> result = await bancoDeDados
        .query(_tablename, where: '$_cpf = ?', whereArgs: [cpf]);
    print('CPF encontrada: ${UserModel.toList(result)}');

    return UserModel.toList(result);
  }

  // #### Update ####
  update(UserModel user, String? newName, int? newIdade) async {
    final Database bancoDeDados = await getDatabase();
    // Map<String, dynamic> userMap = UserModel.toMap(user);
    var itemExists = await find(user.cpf);
    if (itemExists.isNotEmpty) {
      return await bancoDeDados.update(
        _tablename,
        {
          'name': newName != null
              ? newName != 'null'
                  ? newName
                  : user.name
              : user.name,
          'idade': newIdade != null
              ? newIdade != "null"
                  ? newIdade
                  : user.idade
              : user.idade,
        },
        where: 'cpf = ?',
        whereArgs: [user.cpf],
      );
    }
  }

  // #### Delete ####
  delete(String cpf) async {
    print('Deletando CPF: $cpf');
    final Database bancoDeDados = await getDatabase();
    return await bancoDeDados.delete(
      _tablename,
      where: '$_cpf = ?',
      whereArgs: [cpf],
    );
  }
}
