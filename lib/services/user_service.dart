import 'dart:convert';

import 'package:outkeyproject/data/user_dao.dart';

import '../models/user_model.dart';

class UserService {
  UserService();

  Future fetchUser(String cpf) async {
    try {
      List<UserModel> users = await UserDao().find(cpf);

      UserModel user = (users[0]);

      return user;
    } catch (e) {
      return e.toString();
    }
  }

  Future updateUser(UserModel user, String? newName, int? newIdade) async {
    try {
      await UserDao().update(user, newName, newIdade);

      await fetchUser(user.cpf);

      return true;
    } on Exception catch (e) {
      return false;
    }
  }
}
